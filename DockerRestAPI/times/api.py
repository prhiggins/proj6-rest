# Brevet times service

import os
import flask
from flask import Flask, request
from flask_restful import Resource, Api
import pymongo
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.controls

class All(Resource):
	def get(self):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("km", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]
		return {'open': [item['open'] for item in items], 'close': [item['close'] for item in items]}

class OpenOnly(Resource):
	def get(self):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("open", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]
		return {'open': [item['open'] for item in items]}

class CloseOnly(Resource):
	def get(self):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("close", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]
		return {'close': [item['close'] for item in items]}

class AllFormatted(Resource):
	def get(self, format):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("km", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]

		if format == 'json':
			return flask.jsonify({'open': [item['open'] for item in items], 'close': [item['close'] for item in items]})
		elif format == 'csv':
			data = []
			for item in items:
				data += str(item['open']) + ", " + str(item['close']) + ", "
			return data

class OpenOnlyFormatted(Resource):
	def get(self, format):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("open", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]

		if format == 'json':
			return flask.jsonify({'open': [item['open'] for item in items]})
		elif format == 'csv':
			data = []
			for item in items:
				data += item['open'] + ", "
			return data

class CloseOnlyFormatted(Resource):
	def get(self, format):
		top = request.args.get("top")

		items = [item for item in db.rows.find().sort("close", pymongo.ASCENDING)]

		if top != None:
			items = items[:top]

		if format == 'json':
			return flask.jsonify({'close': [item['close'] for item in items]})
		elif format == 'csv':
			data = []
			for item in items:
				data += item['close'] + ", "
			return data


# Create routes
# Another way, without decorators
api.add_resource(All, '/listAll')
api.add_resource(OpenOnly, '/listOpenOnly')
api.add_resource(CloseOnly, '/listCloseOnly')

api.add_resource(AllFormatted, '/listAll/<string:format>/')
api.add_resource(OpenOnlyFormatted, '/listOpenOnly/<string:format>/')
api.add_resource(CloseOnlyFormatted, '/listCloseOnly/<string:format>/')

# Run the application
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5001, debug=True)
