<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>Call to /listAll</h1>
        <ul>
            <?php
            file_get_contents('http://localhost:5001/listAll');
            ?>
        </ul>
		<h1>Call to /listOpenOnly</h1>
		<ul>
			<?php
			file_get_contents('http://localhost:5001/listOpenOnly');
			?>
		</ul>
		<h1>Call to /listCloseOnly</h1>
		<ul>
			<?php
			file_get_contents('http://localhost:5001/listCloseOnly');
			?>
    </body>
</html>
